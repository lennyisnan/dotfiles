" BASIC SETUP:

" enter the current millenium
set nocompatible

" enable syntax and plugins (for netrw)
syntax enable
filetype plugin on

" other stuff
set showcmd

" other stuff
set showcmd
set number
set relativenumber
set complete+=kspell
set mouse=a
set tabstop=4
set shiftwidth=4
set softtabstop=4
set autoindent
set smartindent


" CURSOR SETTINGS:
let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"
"cursor on vim start
let &t_ti .= "\e[2 q"
"cursor change speed
set ttimeout
set ttimeoutlen=1
set ttyfast


" FINDING FILES:

" Search down into subfolders
" Provides tab-completion for all file-related tasks
set path +=**

" Display all matching files when we tab complete
set wildmenu

" NOW WE CAN:
" - Hit tab to :find by partial match
" - Use * to make it fuzzy

" THINGS TO CONSIDER:
" - :b lets you autocomplete any open buffer



" TAG JUMPING:

" Create the `tags` file (may need to install ctags first)
command! MakeTags !ctags -R .

" NOW WE CAN:
" - Use ^] to jump to tag under cursor
" - Use g^] for ambiguous tags
" - Use ^t to jump back up the tag stack

" THINGS TO CONSIDER:
" - This doesn't help if you want a visual list of tags


" AUTOCOMPLETE:

" The good stuff is documented in |ins-completion|

" HIGHLIGHTS:
" - ^x^n for JUST this file
" - ^x^f for filenames (works with our path trick!)
" - ^x^] for tags only
" - ^n for anything specified by the 'complete' option

" NOW WE CAN:
" - Use ^n and ^p to go back and forth in the suggestion list

" FILE BROWSING:
" Tweaks for browsing
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'

" NOW WE CAN:
" - :edit a folder to open a file browser (edit . for tree listing)
" - <CR>/v/t to open in an h-split/v-split/tab
" - check |netrw-browse-maps| for more mappings


" SNIPPETS:

" Read an empty HTML template and move cursor to title, <CR> is enter to go
" back to normal mode
nnoremap ,html :-1read $HOME/.vim/.skeleton.html<CR>3jwf>a
nnoremap ,cmain :-1read $HOME/.vim/.skeleton.cmain<CR>4ja

" NOW WE CAN:
" - Take over the world!
"   (with much fewer keystrokes)

" BUILD INTEGRATION:

" Steal Mr. Bradley's formatter & add it to our spec_helper
" http://philipbradley.net/rspec-into-vim-with-quickfix

" Configure the `make` command to run RSpec
" set makeprg=bundle\ exec\ rspec\ -f\ QuickfixFormatter

" NOW WE CAN:
" - Run :make to run RSpec
"  - :cl to list errors
"  - :cc# to jump to error by number
"  - :cn and :cp to navigate forward and back

" COPY PASTE:
" "<buffer>y/d/p
" es buffer -> a,b... (+ for X11)

" HELP:
" :helpgrep <stuff>
" ^=CTRL es :help ^n (in normal mode), i_^n (in insert mode), c_^n (in cmd
" mode)

" INSERT MODE STUFF:
" CTRL-R -> insert text from a register 
" CTRL-A -> last inserted text
" CTRL-P -> word completion (context aware)
" CTRL-N -> next
" CTRL-X -> completion mode
" CTRL-L -> complete line (context aware)

" FILETYPE SPECIFIC STUFF:
" look insider .vim/ftplugin

" USEFUL STUFF:
" moving around:
" gt -> next tab
" gT -> previous tab
" CTRL+W + (h/j/k/l) -> change split focus

call plug#begin()

Plug 'jiangmiao/auto-pairs'

call plug#end()

colorscheme desert
